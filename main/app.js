function addTokens(input, tokens){
    if(typeof(input)!=='string'){
        throw new Error('Invalid input')
    }

    if(input.length<6){ 
        throw new Error('Input should have at least 6 characters.')
    }

    tokens.forEach(element=>{
        if(typeof(element.tokenName)!=='string'){
            throw new Error('Invalid array format')
        }
    })

    if(!input.includes('...')){
        return input;
    }
    else{
        tokens.forEach(element=>{
        input=input.replace('...', "${" + element.tokenName + "}") // I think the test is wrong here :(
        //but i wanted to pass it, i tried using `${element.tokenName} and it wouldn't pass :( sorry if it was my fault or something
        });
    }
    return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;